/*
 * libc.c 
 */

#include <libc.h>

#include <types.h>

#include <errno.h>

int errno, ret;

void itoa(int a, char *b)
{
  int i, i1;
  char c;
  
  if (a==0) { b[0]='0'; b[1]=0; return ;}
  
  i=0;
  while (a>0)
  {
    b[i]=(a%10)+'0';
    a=a/10;
    i++;
  }
  
  for (i1=0; i1<i/2; i1++)
  {
    c=b[i1];
    b[i1]=b[i-i1-1];
    b[i-i1-1]=c;
  }
  b[i]=0;
}

int strlen(char *a)
{
  int i;
  
  i=0;
  
  while (a[i]!=0) i++;
  
  return i;
}

int write(int fd, char * buffer, int size) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl 12(%ebp), %ecx;"
		"movl 16(%ebp), %edx;"
		"movl $4, %eax;"
		"int $0x80;"
		"movl %eax,errno;"
		"popl %ebx;"
	);
	if (errno < 0) {
		errno = -errno;		
		return -1;
	}
	return errno;
}

void perror() {
	if (errno == EINVAL) {
		char b[] = "Size is an invalid argument";
		write(1, b, sizeof(b));
	}
	else if (errno == EFAULT) {
		char b[] = "Bad address of buffer";
		write(1, b, strlen(b));
	}
	else if (errno == EBADF) {
		char b[] = "File descriptor in bad state";
		write(1, b, sizeof(b));
	}
	else if (errno == EACCES) {
		char b[] = "Permission denied";
		write(1, b, sizeof(b));
	}
	else if(errno == EAGAIN) {
		char b[] = "Try again";
		write(1, b, sizeof(b));
	}
	else if(errno == ENOMEM) {
		char b[] = "Out of memory";
		write(1, b, sizeof(b));
	}
	else if(errno == ESRCH) {
		char b[] = "No such process";
		write(1, b, sizeof(b));
	}
}


int gettime() {
	asm("movl $10, %eax;"
		"int $0x80;"
		"movl %eax, ret;");
	return ret;
}

int getpid(void) {
	asm("movl $20, %eax;"
		"int $0x80;"
		"movl %eax, ret;");
	return ret;
}

int fork(void) {
	asm("movl $2, %eax;"
		"int $0x80;"
		"movl %eax, errno;");
	if (errno < 0) {
		errno = -errno;		
		return -1;
	}
	return errno;
}

void exit(void) {
	asm("movl $1, %eax;"
		"int $0x80;");
}

int get_stats(int pid, struct stats *st) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl 12(%ebp), %ecx;"
		"movl $35, %eax;"
		"int $0x80;"
		"movl %eax, errno;"
		"popl %ebx;"
	);
	if (errno < 0) {
		errno = -errno;		
		return -1;
	}
	return errno;
}

int clone (void (*function)(void), void *stack) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl 12(%ebp), %ecx;"
		"movl $19, %eax;"
		"int $0x80;"
		"movl %eax, errno;"
		"popl %ebx;"
	);
	if (errno < 0) {
		errno = -errno;
		return -1;
	}
	return errno;
}

int sem_init(int n_sem, unsigned int value) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl 12(%ebp), %ecx;"
		"movl $21, %eax;"
		"int $0x80;"
		"movl %eax, errno;"
		"popl %ebx"
	);
	if (errno < 0) {
		errno = -errno;
		return -1;
	}
	return errno;
}

int sem_wait (int n_sem) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl $22, %eax;"
		"int $0x80;"
		"movl %eax, errno;"
		"popl %ebx"
	);
	if (errno < 0) {
		errno = -errno;
		return -1;
	}
	return errno;
}

int sem_signal (int n_sem) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl $23, %eax;"
		"int $0x80;"
		"movl %eax, errno;"
		"popl %ebx"
	);
	if (errno < 0) {
		errno = -errno;
		return -1;
	}
	return errno;
}

int sem_destroy(int n_sem) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl $24, %eax;"
		"int $0x80;"
		"movl %eax, errno;"
		"popl %ebx"
	);
	if (errno < 0) {
		errno = -errno;
		return -1;
	}
	return errno;
}

int read(int fd, char *buf, int count) {
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl 12(%ebp), %ecx;"
		"movl 16(%ebp), %edx;"
		"movl $3, %eax;"
		"int $0x80;"
		"movl %eax, errno;"
		"popl %ebx;"
	);
	if (errno < 0) {
		errno = -errno;
		return -1;
	}
	return errno;
}

void sbrk(int increment){
	asm("pushl %ebx;"
		"movl 8(%ebp), %ebx;"
		"movl $5, %eax;"
		"int $0x80;"
		"movl %eax, ret;"
		"popl %ebx;"
	);
	if(ret == (void*)-1){
		errno = ENOMEM;
	}  
}
