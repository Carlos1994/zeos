/*
 * sched.c - initializes struct for task 0 anda task 1
 */

#include <sched.h>
#include <mm.h>
#include <io.h>

/**
 * Container for the Task array and 2 additional pages (the first and the last one)
 * to protect against out of bound accesses.
 */
union task_union protected_tasks[NR_TASKS+2]
  __attribute__((__section__(".data.task")));

union task_union *task = &protected_tasks[1]; /* == union task_union task[NR_TASKS] */

struct task_struct *init_task; //Prova
struct task_struct *fill; //Prova

struct task_struct *idle_task;
int currentQuantum;

int num_of_access[NR_TASKS];

struct task_struct *list_head_to_task_struct(struct list_head *l)
{
  return list_entry( l, struct task_struct, list);
}

extern struct list_head blocked;

struct semaphore semaphores[20];

struct list_head keyboardqueue;

char KeyBuff[2048];

int posBuff = 0;


/* get_DIR - Returns the Page Directory address for task 't' */
page_table_entry * get_DIR (struct task_struct *t) 
{
	return t->dir_pages_baseAddr;
}

/* get_PT - Returns the Page Table address for task 't' */
page_table_entry * get_PT (struct task_struct *t) 
{
	return (page_table_entry *)(((unsigned int)(t->dir_pages_baseAddr->bits.pbase_addr))<<12);
}


/*int allocate_DIR(struct task_struct *t) 
{
	int pos;

	pos = ((int)t-(int)task)/sizeof(union task_union);

	t->dir_pages_baseAddr = (page_table_entry*) &dir_pages[pos];
	return 1;
}*/

int allocate_DIR(struct task_struct *t, int clone) 
{	
	int i;
	for (i = 0; i < NR_TASKS; ++i) {
		if (num_of_access[i] == 0) {
			t->dir_pages_baseAddr = (page_table_entry*) &dir_pages[i];
			++num_of_access[i];
			return 1;
		}
	}
	return 1;
}

void increment_DIR(struct task_struct *t) {
	int i;
	for (i = 0; i < NR_TASKS; ++i) {
		if(t->dir_pages_baseAddr == /*(page_table_entry*)*/ &dir_pages[i]) {
			++num_of_access[i];
		}
	}
}

int calculate_DIR(struct task_struct *t) {
	int ini, tsk;
	ini = (int)((page_table_entry*) &dir_pages[0]);
	tsk = (int)t->dir_pages_baseAddr;
	return (tsk-ini)/sizeof(dir_pages[0]);
}

void cpu_idle(void)
{
	__asm__ __volatile__("sti": : :"memory");
	
	while(1)
	{
	//printk("idle"); //Prova
	;
	}
}

void init_idle (void)
{
	struct list_head * lh = list_first(&freequeue);
	struct task_struct * tsk = list_head_to_task_struct(lh);
	list_del(lh);
	tsk->PID = 0;
	idle_task = tsk;
	union task_union * tsku = (union task_union *)idle_task;
	tsku->stack[KERNEL_STACK_SIZE-1] = (int)&cpu_idle;
	tsku->stack[KERNEL_STACK_SIZE-2] = 0;
	idle_task->kernel_esp = (int *)&tsku->stack[KERNEL_STACK_SIZE-2];
	allocate_DIR(idle_task, 0);
	
	tsk->quantum = QUANTUM_DEF;
	
	tsk->estad.user_ticks = 0;
	tsk->estad.system_ticks = 0;
	tsk->estad.blocked_ticks = 0;
	tsk->estad.ready_ticks = 0;
	tsk->estad.elapsed_total_ticks = get_ticks();
	tsk->estad.total_trans = 0;
	tsk->estad.remaining_ticks = 0;
	
	tsk->estat = ST_READY;
	
	//Dynamic memory
	tsk->heap_start = NULL;
	tsk->program_break = NULL;
}

void init_task1(void)
{
	struct list_head * lh = list_first(&freequeue);
	struct task_struct * tsk = list_head_to_task_struct(lh);
	list_del(lh);
	tsk->PID = 1;
	allocate_DIR(tsk, 0);
	set_user_pages(tsk);
	union task_union * tsku = (union task_union *)tsk;
	tss.esp0 = (int)&tsku->stack[KERNEL_STACK_SIZE];
	set_cr3(tsk->dir_pages_baseAddr);
	
	tsk->quantum = QUANTUM_DEF;
	
	tsk->estad.user_ticks = 0;
	tsk->estad.system_ticks = 0;
	tsk->estad.blocked_ticks = 0;
	tsk->estad.ready_ticks = 0;
	tsk->estad.elapsed_total_ticks = get_ticks();
	tsk->estad.total_trans = 0;
	tsk->estad.remaining_ticks = QUANTUM_DEF;
	
	tsk->estat = ST_READY;
	
	//Dynamic memory
	tsk->heap_start = NULL;
	tsk->program_break = NULL;
}

void init_semaphores() {
	int i;
	for (i = 0; i < 20; ++i) {
		semaphores[i].count = -1;
		INIT_LIST_HEAD(&semaphores[i].blocked_proc);
		semaphores[i].owner == NULL;
	}
}

void init_sched(){
	INIT_LIST_HEAD(&freequeue);
	int i;
	for (i = 0; i < NR_TASKS; ++i) {
		list_add(&(task[i].task.list), &freequeue);
		task[i].task.num_pag_sbrk = 0;
		task[i].task.not_read = 0;
	}
	INIT_LIST_HEAD(&readyqueue);
	INIT_LIST_HEAD(&keyboardqueue);
	currentQuantum = QUANTUM_DEF;
	init_semaphores();
}

struct task_struct* current()
{
  int ret_value;
  
  __asm__ __volatile__(
  	"movl %%esp, %0"
	: "=g" (ret_value)
  );
  return (struct task_struct*)(ret_value&0xfffff000);
}

void inner_task_switch(union task_union *t) {
	tss.esp0 = (int)&t->stack[KERNEL_STACK_SIZE];
	if (get_DIR(&(t->task)) != get_DIR(current())) set_cr3(get_DIR(&(t->task)));
	int * old = current()->kernel_esp;
	int * new = (t->task).kernel_esp;
	__asm__ __volatile__(
			"movl %%ebp, %0;"
			"movl %1, %%esp;"
			"popl %%ebp;"
			"ret;"
	:
	: "g" (old), "g" (new)
   );
}

void task_switch(union task_union *t) {
	asm("pushl %esi;"
		"pushl %edi;"
		"pushl %ebx;");
	inner_task_switch(t);
	asm("popl %ebx;"
		"popl %edi;"
		"popl %esi;");	
}

int get_quantum(struct task_struct *t) {
	return t->quantum;
}

void set_quantum(struct task_struct *t, int quantum) {
	t->quantum = quantum;
}

void update_process_state_rr (struct task_struct * t, struct list_head * dst_queue){
	//El procès no s'està execetant -> el treiem de la lista
	if (t->PID != current()->PID) {
		list_del(&t->list);
		t->estat = ST_READY;
	}
	//El seu nou estat no es RUNNING -> el fiquem a la llista
	if (dst_queue != NULL) {	
		list_add_tail(&t->list, dst_queue);
		if (t == current() && dst_queue == &readyqueue) update_system_ticks();
		if (dst_queue == &freequeue){
		  t->estat = ST_ZOMBIE;
		  t->heap_start = NULL;
		  t->program_break = NULL;
		  t->num_pag_sbrk = NULL;
		}
		else t->estat = ST_READY;
	}
	else {
		//El nou estat es running -> fem un canvi de contexte
		//current()->estat = ST_READY;
		t->estad.ready_ticks += get_ticks() - t->estad.elapsed_total_ticks;
		t->estad.elapsed_total_ticks = get_ticks();
		t->estat = ST_RUN;
		++t->estad.total_trans;
		currentQuantum = get_quantum(t);
		t->estad.remaining_ticks = currentQuantum;
		
		union task_union *tsku = (union task_union *) t;
		task_switch(tsku);
	}
}

void sched_next_rr(void) {
	union task_union *tu_next;
	if (list_empty(&readyqueue)) tu_next = (union task_union *)idle_task;
	else {
		struct list_head *lh = list_first(&readyqueue);
		tu_next = (union task_union *) list_head_to_task_struct(lh);
		list_del(lh);
	}
	
	tu_next->task.estad.ready_ticks += get_ticks() - tu_next->task.estad.elapsed_total_ticks;
	tu_next->task.estad.elapsed_total_ticks = get_ticks();
	++tu_next->task.estad.total_trans;
	
	tu_next->task.estat = ST_RUN;
	tu_next->task.not_read = 0;
	
	currentQuantum = get_quantum(&tu_next->task);
	tu_next->task.estad.remaining_ticks = currentQuantum;
	task_switch(tu_next);
}

int needs_sched_rr(void){
	return (currentQuantum == 0);
}

void update_sched_data_rr(void){
	--current()->estad.remaining_ticks;
	--currentQuantum;
}

void blocked_to_ready(void) {
	struct list_head *lh = list_first(&keyboardqueue);
	list_head_to_task_struct(lh)->estat = ST_READY;
	list_add_tail(lh, &readyqueue);
}

int is_empty(void) {
	return list_empty(&keyboardqueue);
}

void add_Buffer(char b) {
	KeyBuff[posBuff] = b;
	++posBuff;
}

int pos_ok(void) {
	return (posBuff < 2048);
}
