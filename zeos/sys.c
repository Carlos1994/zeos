/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>

#include <utils.h>

#include <io.h>

#include <mm.h>

#include <mm_address.h>

#include <sched.h>

#include <interrupt.h>

#include <errno.h>

#define LECTURA 0
#define ESCRIPTURA 1

#define N 24

char buff[N];

int pid = 2;

int check_fd(int fd, int permissions)
{
  if (fd!=1) return -EBADF; /*EBADF*/
  if (permissions!=ESCRIPTURA) return -EACCES; /*EACCES*/
  return 0;
}

void update_user_ticks(void) {
	current()->estad.user_ticks += get_ticks() - current()->estad.elapsed_total_ticks;
	current()->estad.elapsed_total_ticks = get_ticks();
}

void update_system_ticks(void) {
	current()->estad.system_ticks += get_ticks() - current()->estad.elapsed_total_ticks;
	current()->estad.elapsed_total_ticks = get_ticks();
}

void update_ready_queue_ticks(void){
	current()->estad.ready_ticks += get_ticks()-current()->estad.elapsed_total_ticks;
	current()->estad.elapsed_total_ticks = get_ticks();
}



int sys_ni_syscall() {
	//update_user_ticks();
	//update_system_ticks();
	return -38; /*ENOSYS*/
}

int sys_getpid() {
	//update_user_ticks();
	//update_system_ticks();
	return current()->PID;
}

int ret_from_fork() {
	return 0;
}

int sys_fork() {
  //update_user_ticks();
  int PID=pid;

  if (list_empty(&freequeue)) {
  	//update_system_ticks();
  	return -EAGAIN;
  }
  
  int vector[NUM_PAG_DATA];
  int page;
  for (page = 0; page < NUM_PAG_DATA; ++page) {
	  vector[page] = alloc_frame();
	  if (vector[page] < 0) {
          --page;
		  while (page >= 0) {
		  	  free_frame(vector[page]);
           	  --page;
          }
          //update_system_ticks();
		  return -ENOMEM;
      }
  }
  
  struct list_head * lh = list_first(&freequeue);
  struct task_struct * tsk = list_head_to_task_struct(lh);
  list_del(lh);		
  
  union task_union *pare = (union task_union *) current();
  union task_union *fill = (union task_union *) tsk;
  copy_data(pare, fill, sizeof(union task_union));
  
  tsk->PID = pid;
  ++pid;
 
  allocate_DIR(&fill->task, 0);


  page_table_entry *pareT = get_PT(&pare->task);
  page_table_entry *fillT = get_PT(&fill->task);	
  int frame;
  for (page = 0; page < NUM_PAG_KERNEL+NUM_PAG_CODE; ++page) {
  	  frame = get_frame(pareT, page);
      set_ss_pag(fillT, page, frame);
  }

  int pos = NUM_PAG_KERNEL + NUM_PAG_CODE + NUM_PAG_DATA + 1;
  for (page = 0; page < NUM_PAG_DATA + current()->num_pag_sbrk; ++page) {
  	  set_ss_pag(fillT, NUM_PAG_KERNEL + NUM_PAG_CODE + page, vector[page]);
	  set_ss_pag(pareT, pos+page, vector[page]);
	  copy_data((void *)((NUM_PAG_KERNEL + NUM_PAG_CODE + page)*PAGE_SIZE), (void *)((pos + page)*PAGE_SIZE), PAGE_SIZE);
      del_ss_pag(pareT, pos+page); 	
  }

  set_cr3(get_DIR(&pare->task));
  fill->task.estad.user_ticks = 0;
  fill->task.estad.system_ticks = 0;
  fill->task.estad.blocked_ticks = 0;
  fill->task.estad.ready_ticks = 0;
  fill->task.estad.total_trans = 0;
  fill->task.estat = ST_READY;
  fill->stack[KERNEL_STACK_SIZE-18] = (int)&ret_from_fork;
  fill->stack[KERNEL_STACK_SIZE-19] = 0;
  fill->task.kernel_esp = (int *)&fill->stack[KERNEL_STACK_SIZE-19];
  
  list_add_tail(&(fill->task.list), &readyqueue);
  
  //update_system_ticks();
  return PID;
}

void sys_exit() {
	//update_user_ticks(); 
	int i, pos;
	for (i = 0; i < 20; ++i) {
		if(semaphores[i].owner == current()) {
			sys_sem_destroy(i);
			semaphores[i].owner = NULL;
		}
	}
	pos = calculate_DIR(current());
	--num_of_access[pos];
	if (num_of_access[pos] <= 0) free_user_pages(current());
	update_process_state_rr(current(), &freequeue);
	sched_next_rr();
}

int sys_write(int fd, char * buffer, int size) {
	//update_user_ticks();	
	int x = check_fd(fd, ESCRIPTURA);
	if (x < 0) {
		update_system_ticks();
		return x;
	}
	if (buffer == NULL) {
		update_system_ticks();
		return -EFAULT;
	}
	if (access_ok(1, buffer, size) == 0 || size < 0) {
		update_system_ticks();
		return -EINVAL;
	}

	int n = size;
	int i = 0;
	while (n > N) {
		copy_from_user(buffer+(N*i), buff, N);
		sys_write_console(buff, N);	
		n -= N;
		++i;		
	}
	if (n > 0) {
		copy_from_user(buffer+(N*i), buff, n);
		sys_write_console(buff, n);	
	}
	//update_system_ticks();
	return size;	
}

int sys_gettime() {
	//update_user_ticks();
	//update_system_ticks();
	return zeos_ticks;
}

int sys_getstats(int pid, struct stats *st) {
	//update_user_ticks();
	
	if (pid < 0) {
		//update_system_ticks();
		return -EINVAL;
	}
	
	if (!access_ok(VERIFY_WRITE, st, sizeof(struct stats))) {
		//update_system_ticks();
		return -EFAULT;
	}
	
	int i;
	for (i = 0; i < NR_TASKS; ++i) {
		if (task[i].task.PID == pid && task[i].task.estat != ST_ZOMBIE) {
			copy_to_user(&task[i].task.estad, st, sizeof(struct stats));
			//update_system_ticks();
			return 0;
		}
	}
	//update_system_ticks();
	return -ESRCH;
}

int sys_clone (void (*function)(void), void *stack) {
	//update_user_ticks();
	int PID = pid; 
	
	if (list_empty(&freequeue)) {
		//update_system_ticks();
		return -EAGAIN;
	}
	if(!access_ok(VERIFY_READ, function, 4) || !access_ok(VERIFY_WRITE,stack,4)){
		return -EFAULT;
	}
	
	struct list_head * lh = list_first(&freequeue);
	struct task_struct * tsk = list_head_to_task_struct(lh);
	list_del(lh);		
	
	union task_union *pare = (union task_union *) current();
	union task_union *fill = (union task_union *) tsk;
	copy_data(pare, fill, sizeof(union task_union));
	
	tsk->PID = pid;
	++pid;
	
	increment_DIR(tsk);
	++num_of_access[calculate_DIR(&fill->task)];
	
	fill->task.estad.user_ticks = 0;
	fill->task.estad.system_ticks = 0;
	fill->task.estad.blocked_ticks = 0;
	fill->task.estad.ready_ticks = 0;
	fill->task.estad.total_trans = 0;
	fill->task.estat = ST_READY;
	fill->stack[KERNEL_STACK_SIZE-18] = (int)&ret_from_fork;
	fill->stack[KERNEL_STACK_SIZE-19] = 0;
	fill->task.kernel_esp = (int *)&fill->stack[KERNEL_STACK_SIZE-19];
	
	fill->stack[KERNEL_STACK_SIZE-2] = (int) stack; //esp = &stack
	fill->stack[KERNEL_STACK_SIZE-5] = (int) function; //eip = &function
	
	list_add_tail(&(fill->task.list), &readyqueue);
	
	//update_system_ticks();
	return PID;
}

int sys_sem_init(int n_sem, unsigned int value) {
	if(n_sem < 0 || n_sem >19 || value < 0 || value > 10){
	  return -EINVAL;
	}
	else if(semaphores[n_sem].owner != NULL) return -EBUSY;
	semaphores[n_sem].count = value;
	semaphores[n_sem].owner = current();
	INIT_LIST_HEAD(&semaphores[n_sem].blocked_proc);
	return 0;
}

int sys_sem_wait(int n_sem) {
    if(n_sem < 0 || n_sem >19 || semaphores[n_sem].owner == NULL){
      return -EINVAL;
    }
    if(semaphores[n_sem].count <= 0){
    	current()->info_sem = 0;
		current()->estat = ST_BLOCKED;
		list_add_tail(&current()->list, &semaphores[n_sem].blocked_proc);
		sched_next_rr();     
    }
    else --semaphores[n_sem].count;
	return current()->info_sem;
}

int sys_sem_signal(int n_sem) {
    if(n_sem < 0 || n_sem >19 || semaphores[n_sem].owner == NULL ){
      return -EINVAL;
    }
    if(!list_empty(&semaphores[n_sem].blocked_proc)){
		struct list_head * cua = list_first(&semaphores[n_sem].blocked_proc);
		struct task_struct * tasca = list_head_to_task_struct(cua);
		list_del(cua);
		tasca -> estat = ST_READY;
		list_add_tail(cua, &readyqueue);
    }
    else ++semaphores[n_sem].count;
    //++semaphores[n_sem].count;
	return 0;
}

int sys_sem_destroy(int n_sem) {
	if((n_sem < 0 || n_sem >19 || semaphores[n_sem].owner == NULL) ){
    	return -EINVAL;
    }
    else if(semaphores[n_sem].owner != current()) return -EPERM;
	struct list_head *lh;
	struct task_struct *tsk;
	while (!list_empty(&semaphores[n_sem].blocked_proc)) {
		lh = list_first(&semaphores[n_sem].blocked_proc);
		tsk = list_head_to_task_struct(lh);
		list_del(lh);
		tsk->estat = ST_READY;
		tsk->info_sem = -1;
		list_add_tail(lh, &readyqueue);
	}
	semaphores[n_sem].owner = NULL;
	return 0;
}

int sys_read_keyboard(char *buff, int count) {
	//KeyBuff[0] = 'a';
	//posBuff += 1;
	if (list_empty(&keyboardqueue)) {
		printk("if 1\n");
		if ((posBuff - count) >= 0) {
			printk("if 2\n");
			copy_to_user(&KeyBuff[posBuff-count], buff, count);
			return count;
		}
		else {
			printk("else 1\n");
			copy_to_user(&KeyBuff[0], buff, posBuff);
			current()->not_read = count-posBuff;
			list_add(&current()->list, &keyboardqueue);
			sched_next_rr();
			sys_read_keyboard(buff, current()->not_read);
		}
	}
	else {
		printk("else 2\n");
		current()->estat = ST_BLOCKED;
		list_add_tail(&current()->list, &keyboardqueue);
		current()->not_read = count;
		sched_next_rr();
		sys_read_keyboard(buff, current()->not_read);
	}
}

int sys_read(int fd, char *buff, int count) {
	if (buff == NULL) return -EFAULT;
	printk("Buffer not null\n");
	if (check_fd(fd, LECTURA) < 0) return -EBADF;
	printk("FD OK\n");
	if (!access_ok(VERIFY_WRITE, buff, count)) return -EFAULT;
	printk("acces ok\n");
	if (count < 0) return -EINVAL;
	printk("count major o igual que 0\n");
	
	return sys_read_keyboard(buff, count);
} 

void *sys_sbrk(int increment){
	page_table_entry *curr = get_PT(current());
	int frame, pos;
	if (current()->heap_start == NULL) {
		frame = alloc_frame();
		pos = NUM_PAG_KERNEL + NUM_PAG_CODE + NUM_PAG_DATA;
		set_ss_pag(curr, pos, frame);
		current()->heap_start = (void *)((NUM_PAG_KERNEL + NUM_PAG_CODE)*PAGE_SIZE);
		current()->program_break = current()->heap_start;
		current()->num_pag_sbrk = 1;
	}
	void *heap = current()->heap_start;
	void *pbreak = current()->program_break;
	
	int pag_act = (pbreak - heap) / 1024;
	int pag_inc = (pbreak + increment - heap) / 1024;
	
	if (/*(pbreak + increment) < heap || */((pbreak - heap + increment) >= 4198400)) {
		return (void *) -1;
	}
	else if ((pbreak + increment) < heap) {
		printk("heap mes petit");
		int i;
		pos = NUM_PAG_KERNEL + NUM_PAG_CODE + NUM_PAG_DATA;
		for (i = 0; i < current()->num_pag_sbrk; ++i) {
			frame = get_frame(curr,pos);
			del_ss_pag(curr, pos);
			free_frame(frame);
			++pos;
		}
		current()->num_pag_sbrk = 0;
		void *tmp = (current()->program_break);
		current()->program_break = heap;
		return (void *) (tmp);
	}
	else {
		if (pag_act != pag_inc) {
			int i;
			pos = NUM_PAG_KERNEL + NUM_PAG_CODE + NUM_PAG_DATA + pag_act;
			int vector[pag_inc-pag_act];			
			for (i = 1; i < (pag_inc-pag_act); ++i) {
				vector[i] = alloc_frame();
				if (vector[i] < 0) {
					int j;
					for (j = i-1; j >= 0; --j) {
						free_frame(vector[j]);
						del_ss_pag(curr, pos + j);
						--(current()->num_pag_sbrk);
					}
					return (void *) -1;
				}
				set_ss_pag(curr, pos + i, vector[i]);
				++(current()->num_pag_sbrk);
			}
			for (i = pag_act; i >= pag_inc; --i) {
				frame = get_frame(curr,pos);
				del_ss_pag(curr, pos);
				free_frame(frame);
				--pos;
				--(current()->num_pag_sbrk);
			}
		}
		current()->program_break += increment;
	}
	
	return (void *) (current()->program_break - increment);
    
}
