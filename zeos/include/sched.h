/*
 * sched.h - Estructures i macros pel tractament de processos
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include <list.h>
#include <types.h>
#include <mm_address.h>
#include <stats.h>
#include <utils.h>

#define NR_TASKS      10
#define KERNEL_STACK_SIZE	1024
#define QUANTUM_DEF		30

enum state_t { ST_RUN, ST_READY, ST_BLOCKED, ST_ZOMBIE };

struct dyn_mem {
	void *heap_start;
	void *program_break;
	int num_pag_sbrk;
};

struct task_struct {
  int PID;			/* Process ID. This MUST be the first field of the struct. */
  page_table_entry * dir_pages_baseAddr;
  struct list_head list;
  int *kernel_esp;
  unsigned int quantum;
  struct stats estad;
  enum state_t estat;
  int info_sem;
  void *heap_start;
  void *program_break;
  int num_pag_sbrk;
  int not_read;
};

union task_union {
  struct task_struct task;
  unsigned long stack[KERNEL_STACK_SIZE];    /* pila de sistema, per procés */
};

extern union task_union protected_tasks[NR_TASKS+2];
extern union task_union *task; /* Vector de tasques */
extern struct task_struct *idle_task;
extern struct task_struct *init_task; //Prova
extern struct task_struct *fill; //Prova

struct list_head freequeue;

struct list_head readyqueue;

extern struct list_head keyboardqueue;

struct semaphore {
	int count;
	struct list_head blocked_proc;
	struct task_struct *owner;
};

extern struct semaphore semaphores[20];

extern char KeyBuff[2048];

extern int posBuff;



#define KERNEL_ESP(t)       	(DWord) &(t)->stack[KERNEL_STACK_SIZE]

#define INITIAL_ESP       	KERNEL_ESP(&task[1])

/* Inicialitza les dades del proces inicial */
void init_task1(void);

void init_idle(void);

void init_sched(void);

struct task_struct * current();

void task_switch(union task_union*t);

struct task_struct *list_head_to_task_struct(struct list_head *l);

int allocate_DIR(struct task_struct *t, int clone);

page_table_entry * get_PT (struct task_struct *t) ;

page_table_entry * get_DIR (struct task_struct *t) ;

/* Headers for the scheduling policy */
void sched_next_rr();
void update_process_state_rr(struct task_struct *t, struct list_head *dest);
int needs_sched_rr();
void update_sched_data_rr();

int get_quantum(struct task_struct *t);
void set_quantum(struct task_struct *t, int quantum);

void update_system_ticks(void);

void blocked_to_ready(void);

int is_empty(void);

void add_Buffer(char b);

int pos_ok(void);

#endif  /* __SCHED_H__ */
