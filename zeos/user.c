#include <libc.h>

char buff[24];

int pid, ret;
int ah;
	char b[] = "Funciono";
long inner(long n)
{
	int i;
	long suma;
	suma = 0;
	for(i = 0; i<n; i++) suma = suma+i;
	return suma;
}

long outer(long n)
{
	int i;
	long acum;
	acum = 0;                                              
	for(i = 0; i<n; i++) acum = acum+inner(i);
	return acum;
}

int add(int par1, int par2) {
	asm("movl $0, %eax;"
	    "addl 0x8(%ebp), %eax;"
	    "addl 0xc(%ebp), %eax;"
	    "movl %eax, ret;");
	return ret;
}

void prova(void) {
	sem_wait(0);
	char b[] = "Funciono";
	write(1, b, strlen(b));
	sem_signal(1);
	exit();
}

int __attribute__ ((__section__(".text.main")))
  main(void)
{
    /* Next line, tries to move value 0 to CR3 register. This register is a privileged one, and so it will raise an exception */
     /* __asm__ __volatile__ ("mov %0, %%cr3"::"r" (0) ); */
	
	runjp();
	//runjp_rank(4,4);

	while(1) {
	}
	return 0;
}
